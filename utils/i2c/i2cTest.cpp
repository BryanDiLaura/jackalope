//Bryan DiLaura
//quick hardware bringup script for the MCP4728

#include <assert.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <linux/i2c-dev.h>

#include <iostream>
#include <chrono>

//some globals for ease of use...
int file;
char input; 
bool running = true;
uint8_t outBuff[24]; 
int retCode; 
using namespace std;


//-----------------------------------------------------------
void printOpts(void){
	cout << "what would you like to do?" << endl;
	cout << "m = max voltage" << endl;
	cout << "s = smallest voltage" << endl;
	cout << "r = read" << endl;
	cout << "v = set Vref to external" << endl;
	cout << "p = profile" << endl;
	cout << "q = quit" << endl;
	cout << ">";
	cin >> input;
	cout << endl << endl;
}

//-----------------------------------------------------------
void setDacMax(void){
	for(int i=0; i<8; ++i){
		if(i % 2 == 0) outBuff[i] = 0x0F;
		else outBuff[i] = 0xFF;
	}
	retCode = write(file, outBuff, 8);
}

//-----------------------------------------------------------
void setDacMin(void){
	for(int i=0; i<8; ++i){
		outBuff[i] = 0;
	}
	retCode = write(file, outBuff, 8);
}

//-----------------------------------------------------------
void readDacVals(void){
	retCode = read(file, outBuff, 24);
	cout << "got: " << endl;
	for(int i=0; i<24; ++i){
		if(i % 6 == 0) cout << "-----------" << endl;
		cout << i << ": " << hex << unsigned(outBuff[i]) << endl;
	}
	cout << "with Return = " << retCode << endl;
}

//-----------------------------------------------------------
void profileDac(void){
	using namespace chrono;
	
	cout << "flip-flopping 100,000 times as fast as possible..." << endl;
	cout << "continue? y/n" << endl;
	cout << ">";
	cin >> input;
	switch(input){
		case 'y':
			cout << "beginning...now!" << endl;
			break;
		case 'n':
		default:
			cout << "returning" << endl;
			return;
	}

	auto start = high_resolution_clock::now();

	for(long i = 0; i < 100000; ++i){	
		//setDacMax();
		//setDacMin();
	}

	auto stop = high_resolution_clock::now();

	auto timeSpan = duration_cast<duration<double> > (stop - start);
	
	cout << "It took " << timeSpan.count() << "seconds." << endl;
	cout << "Or in other words " << 100000.0 / timeSpan.count() << "Hz" << endl;
}

//-----------------------------------------------------------
int main(void){

	//open the i2c device file and make sure it works...
	file = open("/dev/i2c-2", O_RDWR);
	assert(file != -1);
	assert(ioctl(file, I2C_SLAVE, 0x60) == 0);

	while(running){
		printOpts();	

		switch(input){
			case 'm':
				setDacMax();
				cout << "wrote 0xFFF's. Return = " << retCode << endl;	
				break;

			case 's':
				setDacMin();
				cout << "wrote 0's. Return = " << retCode << endl;	
				break;

			case 'r':
				readDacVals();				
				break;

			case 'v':
				outBuff[0] = 0x80;
				retCode = write(file, outBuff, 1);
				cout << "set to Vdd. Return = " << retCode << endl;
				break;

			case 'p':
				profileDac();
				break;

			case 'q':
				cout << "quitting..." << endl;	
				running = false;
				break;
			default:
				cout << "invalid option!" << endl;
				break;
		}

		cout << "------------------------" << endl << endl;

	}


	close(file);
	return 0;
}

