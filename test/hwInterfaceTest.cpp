#include "catch.hpp"

#include "HwInterface.h"
#include "parameters.h"


TEST_CASE("Test Dac Value", "[HW]"){
    DacValue val;
    const unsigned int maxValue = pow(2, globals::DAC_BITS) - 1;

    SECTION("Check initialization"){
        CHECK(val.getPositive() == maxValue/2);
        CHECK(val.getNegative() == maxValue/2);
    }

    SECTION("Setting works as expected"){
        REQUIRE_NOTHROW(val.set(0));
        CHECK(val.getPositive() == 0);
        CHECK(val.getNegative() == maxValue);

        REQUIRE_NOTHROW(val.set(maxValue));
        CHECK(val.getPositive() == maxValue);
        CHECK(val.getNegative() == 0);

        REQUIRE_NOTHROW(val.set(maxValue/2));
        CHECK(val.getPositive() == maxValue/2);
        CHECK(val.getNegative() == maxValue/2 + 1); //off by one beause of 0-indexing
    }

    SECTION("Corner cases"){
        REQUIRE_THROWS(val.set(50000000));
    }
}

TEST_CASE("Test HwInterface", "[HW]"){

    //I couldn't find a good way to test this because of the
    //ioctl calls. The actual hardware should be used to verify
    //this code, along with a scope. If it is decided later that 
    //I want to actually test this for real, I will figure
    //something out then. 
    CHECK(true);

    // //start with a clean slate
    // remove("./testFile.bin");
    // auto fd = fopen("./testFile.bin", "wb");
    // fclose(fd);

    // HwInterface hw("./testFile.bin");

    // SECTION("Check writing"){
    //     hw.pointAndCommit(0, 0);
    //     fd = fopen("./testFile.bin", "rb");
    //     uint16_t buff;

    //     fread(&buff, 1, 1, fd);
    //     CHECK(buff == 0b01100000);

    //     fread(&buff, 2, 1, fd);
    //     CHECK(buff == 0);

    //     fread(&buff, 2, 1, fd);
    //     CHECK(buff == 4096);

    //     fread(&buff, 2, 1, fd);
    //     CHECK(buff == 0);

    //     fread(&buff, 2, 1, fd);
    //     CHECK(buff == 4096);
    // }

    // remove("./testFile.bin");
}