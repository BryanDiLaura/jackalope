#include "catch.hpp"

#include <opencv2/core.hpp>
#include "Frame.h"
using namespace cv;

TEST_CASE("Test Frame Class"){
    Frame f;
    REQUIRE(f.frameLength() == 0);
    Point p(100, 200);

    SECTION("Adding and clearing points"){
        f.addPoint(p);
        REQUIRE(f.frameLength() == 1);
        REQUIRE(f.getPoint(0) == p);
        f.clear();
        REQUIRE(f.frameLength() == 0);
    }

    SECTION("Getting points corner case exceptions"){
        REQUIRE_THROWS(f.getPoint(100));
        f.addPoint(p);
        REQUIRE_NOTHROW(f.getPoint(0));
        REQUIRE_THROWS(f.getPoint(1));
    }

    SECTION("Removing points corner case exceptions"){
        f.addPoint(p);
        f.addPoint(Point(300,400));
        REQUIRE(f.frameLength() == 2);
        REQUIRE_THROWS(f.removePoint(5));
        REQUIRE_NOTHROW(f.removePoint(0));
        REQUIRE(f.getPoint(0) == Point(300,400));
    }

    SECTION("Point selections manipulation"){
        std::vector<Point> points;
        for(int i=0; i<10; ++i){
            points.push_back(Point(i, i));
        }

        SECTION("adding to the end"){
            f.addPoint(p);
            f.addSelection(points);
            REQUIRE(f.frameLength() == 11);
            REQUIRE(f.getPoint(10) == Point(9,9));
        }

        SECTION("inserting in the middle"){
            for(int i=0; i<3; ++i){
                f.addPoint(p);
            }
            REQUIRE_NOTHROW(f.addSelection(points, 1));
            REQUIRE(f.getPoint(0) == p);
            REQUIRE(f.getPoint(1) == Point(0,0));
            REQUIRE(f.getPoint(11) == p);

            REQUIRE_THROWS(f.addSelection(points, 1000));
        }

        SECTION("removing sections"){
            f.addSelection(points);
            REQUIRE_THROWS(f.clearSection(100, 2));
            REQUIRE_THROWS(f.clearSection(1, 200));
            REQUIRE_THROWS(f.clearSection(100, 200));
            REQUIRE_THROWS(f.clearSection(2, 1));

            REQUIRE_NOTHROW(f.clearSection(1, 3));
            REQUIRE(f.getPoint(1) == Point(3,3));
            
            //range of 0 won't do anything
            REQUIRE_NOTHROW(f.clearSection(1,1));
            REQUIRE(f.getPoint(1) == Point(3,3));
        }

        SECTION("replacing sections"){
            std::vector<Point> pointsTimes10;
            for(auto p : points){
                pointsTimes10.push_back(Point(p.x*10, p.y*10));
            }

            f.addSelection(points);
            REQUIRE(f.frameLength() == 10);
            
            REQUIRE_THROWS(f.replaceSection(100, 2, pointsTimes10));
            REQUIRE_THROWS(f.replaceSection(1, 200, pointsTimes10));
            REQUIRE_THROWS(f.replaceSection(100, 200, pointsTimes10));
            REQUIRE_THROWS(f.replaceSection(2, 1, pointsTimes10));

            REQUIRE_NOTHROW(f.replaceSection(1, 3, pointsTimes10));
            REQUIRE(f.frameLength() == 18);
            REQUIRE(f.getPoint(2) == Point(10,10));
            REQUIRE(f.getPoint(11) == Point(3,3));
        }
    }

}
