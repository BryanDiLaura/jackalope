#include "catch.hpp"

#include "Show.h"
#include "Frame.h"

TEST_CASE("Test Show Class", "[Show]"){
	Show s;

	SECTION("Show state manipulation"){
		SECTION("Check default values"){
			REQUIRE(s.showLength() == 0);
			REQUIRE(s.currentFrameNum() == 0);
			REQUIRE(s.frameRepeats() == 1);
			REQUIRE(s.getPlaybackState() == Show::SETUP);
		}

		SECTION("Check pausing"){
			s.pause();
			REQUIRE(s.getPlaybackState() == Show::PAUSE);
		}

		SECTION("Check stopping"){
			s.stop();
			REQUIRE(s.getPlaybackState() == Show::STOP);
		}

		SECTION("Check playing"){
			//note this will start the playback thread...
			REQUIRE_NOTHROW(s.play());
			REQUIRE(s.getPlaybackState() == Show::PLAY);
			s.stop();
		}
	}

	SECTION("Show frame manipulation"){
		SECTION("Check that we can add single frames"){
			REQUIRE(s.showLength() == 0);
			Frame f;
			s.addFrame(f);
			REQUIRE(s.showLength() == 1);
		}

		SECTION("after adding frames..."){
			Frame f;
			for(int i=0; i<5; ++i){
				s.addFrame(f);
			}
			REQUIRE(s.showLength() == 5);

			SECTION("can add a frame to a specific locaiton"){
				
				REQUIRE_NOTHROW(s.addFrame(f, 3));
				REQUIRE(s.showLength() == 6);

				REQUIRE_THROWS(s.addFrame(f, 10));
			}

			SECTION("can remove frames"){
				REQUIRE_NOTHROW(s.removeFrame(2));
				REQUIRE(s.showLength() == 4);
				REQUIRE_THROWS(s.removeFrame(10));
			}

			SECTION("can clear the show"){
				s.clear();
				REQUIRE(s.showLength() == 0);
			}
		}
	}
}