#ifndef JACKALOPEASSERT_H
#define JACKALOPEASSERT_H

#include <assert.h>

//comment out below to turn standard exceptions back on. If uncommented, 
//exceptions will be thrown.
#define UNIT_TEST

//in order to work with catch2, override assert to just throw an
//exception, rather than terminating
#ifdef UNIT_TEST
    #include <stdexcept>
    #include <cstdio>
    #define NDEBUG
    #undef assert
    #define assert(expression) if(!(expression)){char s[100]; \
        snprintf(s, 100, "assertion failed at %s::%d\n", __FILE__, __LINE__);\
        throw std::runtime_error(s);}
#endif


#endif /* JACKALOPEASSERT_H */
