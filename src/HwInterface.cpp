
#include "HwInterface.h"
#include "jackalopeAssert.h"

#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <linux/i2c-dev.h>
#include <netinet/in.h>

#include <thread>
#include <chrono>
using namespace std::this_thread;
using namespace std::chrono;

const std::string GPIO_EXPORT_FILE("/sys/class/gpio/export");
const std::string GPIO_UNEXPORT_FILE("/sys/class/gpio/unexport");
const std::string LASER_GPIO_FILE_BASE("/sys/class/gpio/gpio" + globals::LASER_PIN_STR);
const std::string LDAC_GPIO_FILE_BASE("/sys/class/gpio/gpio" + globals::LDAC_PIN_STR);

//-----------------------------------------------------------------
uint16_t DacValue::getPositive(void){
    return mPositive;
}

//-----------------------------------------------------------------
uint16_t DacValue::getNegative(void){
    return mNegative;
}

//-----------------------------------------------------------------
void DacValue::set(const unsigned int val){
    assert(val <= mMax);
    mPositive = val;
    mNegative = mMax - val;
}

//-----------------------------------------------------------------
HwInterface::HwInterface(const std::string fileLocation)
    : mFileLocation(fileLocation){
    
    //grab the file. Either a device interface file, or actual file in unit test
    mFileDescriptor = open(fileLocation.c_str(), O_RDWR);
    assert(mFileDescriptor != -1);

    //setup the i2c interface
    assert(ioctl(mFileDescriptor, I2C_SLAVE_FORCE, 0b01100000) == 0);

    //make sure that the vRefs are set properly (so we are using the external voltage)
    mOutBuff[0] = 0x80;
    write(mFileDescriptor, mOutBuff, 1);

    gpioSetup();

}

//-----------------------------------------------------------------
HwInterface::~HwInterface(void){

    setLaser(false);

    close(mFileDescriptor);
    close(mLaserFileDescriptor);
    close(mLdacFileDescriptor);

    const int gpioUnexportFile = open(GPIO_UNEXPORT_FILE.c_str(), O_WRONLY);
    write(gpioUnexportFile, globals::LASER_PIN_STR.c_str(), globals::LASER_PIN_STR.size());
    write(gpioUnexportFile, globals::LDAC_PIN_STR.c_str(), globals::LDAC_PIN_STR.size());
    close(gpioUnexportFile);
}

//-----------------------------------------------------------------
void HwInterface::gpioSetup(void){
    //export GPIO files if they don't already exist
    const int gpioExportFile = open(GPIO_EXPORT_FILE.c_str(), O_WRONLY);
    assert(gpioExportFile != -1);
    write(gpioExportFile, globals::LASER_PIN_STR.c_str(), globals::LASER_PIN_STR.size());
    write(gpioExportFile, globals::LDAC_PIN_STR.c_str(), globals::LDAC_PIN_STR.size());
    close(gpioExportFile);

    //need to sleep for a bit for some reason...
    sleep_for(milliseconds(200));

    //set them to 'output'
    const std::string setting("out");
    const int outputFile = open((LASER_GPIO_FILE_BASE + "/direction").c_str(), O_WRONLY);
    assert(outputFile != -1);
    write(outputFile, setting.c_str(), setting.size());
    close(outputFile);

    const int outputFile2 = open((LDAC_GPIO_FILE_BASE + "/direction").c_str(), O_WRONLY);
    assert(outputFile2 != -1);
    write(outputFile2, setting.c_str(), setting.size());
    close(outputFile2);

    //open the value files, to stay open for the duration of the class lifetime
    mLaserFileDescriptor = open((LASER_GPIO_FILE_BASE + "/value").c_str(), O_RDWR);
    mLdacFileDescriptor = open((LDAC_GPIO_FILE_BASE + "/value").c_str(), O_RDWR);

    //make sure that the laser starts low
    write(mLaserFileDescriptor, "0", 1);
    mLaserStatus = false;

    //make sure ldac starts high
    write(mLdacFileDescriptor, "1", 1);
}

//-----------------------------------------------------------------
void HwInterface::pointAndCommit(const unsigned int x, const unsigned int y){
    point(x, y);
    commit();
}

//-----------------------------------------------------------------
void HwInterface::point(const unsigned int x, const unsigned int y){

    const unsigned int candidateX = x*mXStep;
    const unsigned int candidateY = y*mYStep;

    assert(candidateX < mXLim);
    assert(candidateY < mYLim);

    mCurX.set(candidateX);
    mCurY.set(candidateY);
}

//-----------------------------------------------------------------
void HwInterface::commit(void){
    //note that I'm taking advantage of the fact that there is
    //zero padding on these numbers to handle the command type
    //(C2 = 0, C1 = 0 means "fast write command"), and the power-down
    //bits (PD1 = 0, PD0 = 0 means "normal mode")

    mOutBuff[0] = htons(mCurX.getPositive());
    mOutBuff[1] = htons(mCurX.getNegative());
    mOutBuff[2] = htons(mCurY.getPositive());
    mOutBuff[3] = htons(mCurY.getNegative());

    if(write(mFileDescriptor, mOutBuff, 8) != 8){
        //TODO: maybe retry here? We'll see how reliable it is first
    }

    //toggle ldac to load the values instantaneously
    write(mLdacFileDescriptor, "0", 1);
    write(mLdacFileDescriptor, "1", 1);

    //allow the movement to actually happen before we do anything else
    sleep_for(microseconds(globals::MOVE_WAIT_US));
}

//-----------------------------------------------------------------
void HwInterface::toggleLaser(void){
    setLaser(!mLaserStatus);    
}

//-----------------------------------------------------------------
void HwInterface::setLaser(bool en){
    if (en){
        write(mLaserFileDescriptor, "1", 1);
    }
    else{
        write(mLaserFileDescriptor, "0", 1);
    }
    mLaserStatus = en;

    //let the laser actually turn on/off before doing anything else
    sleep_for(microseconds(globals::LASER_POWER_WAIT_US));
}

//-----------------------------------------------------------------
bool HwInterface::laserStatus(void){
    return mLaserStatus;
}