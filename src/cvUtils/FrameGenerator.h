#ifndef FRAMEGENERATOR_H
#define FRAMEGENERATOR_H

#include "Frame.h"

#include <vector>
#include <string>
#include <memory>
#include <opencv2/opencv.hpp>

class FrameGenerator{
public:
    FrameGenerator(std::string filename);
    ~FrameGenerator();

    std::shared_ptr<Frame> getFrame(void);

private:
    cv::VideoCapture mVideoCapture;
    cv::Mat mFrame;

};


#endif /* FRAMEGENERATOR_H */
