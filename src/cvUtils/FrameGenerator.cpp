#include "FrameGenerator.h"

#include <iostream>

using namespace cv;
using namespace std;

FrameGenerator::FrameGenerator(string filename){
    mVideoCapture.open(0);
    assert(mVideoCapture.isOpened());
}

FrameGenerator::~FrameGenerator(void){
    mVideoCapture.release();
}

std::shared_ptr<Frame> FrameGenerator::getFrame(void){
    mVideoCapture >> mFrame;

    cout << "got frame (" << mFrame.cols << "x" << mFrame.rows << ")" << endl;
    Mat dest;

    //convert to grayscale and blur
    cvtColor(mFrame, mFrame, COLOR_BGR2GRAY);
    bilateralFilter(mFrame, mFrame, 9, 75, 75);

    cout << "converted to grayscale and blurred" << endl;

    //use otsu thresholding
    threshold(mFrame, mFrame, 0, 255, THRESH_BINARY + THRESH_OTSU);

    cout << "thresholded" << endl;

    //get contours
    vector<vector<cv::Point> > contours;
    findContours(mFrame, contours, RETR_TREE, CHAIN_APPROX_SIMPLE);

    cout << "found contours" << endl;

    //do polygon approximation
    double epsilon;
    vector<vector<cv::Point> > approximations;
    approximations.reserve(contours.size());
    for(int i=0; i<contours.size(); ++i){
        epsilon = 0.001 * arcLength(contours[i], true);
        approxPolyDP(contours[i], approximations[i], epsilon, isContourConvex(contours[i]));
    }

    cout << "did approximations" << endl;

    //create Frame
    return std::make_shared<Frame>(approximations);

    
}