#ifndef SHOW_H
#define SHOW_H
/*
 * Jackalope - Laser light show control software
 * Copyright (C) 2019  Bryan DiLaura
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * @file Show.h
 * @author Bryan DiLaura (dilauraba@gmail.com)
 * @brief A class that contains information about the show. Includes functionality like
 *        imposing limits on where we can write to. Owns the hw interface
 * @version 0.1
 * @date 2019-02-21
 * 
 * @copyright Copyright (c) 2019
 * 
 */

#include "Frame.h"
#include "parameters.h"

#include <memory>
#include <vector>
#include <string>
#include <atomic>
#include <thread>
#include <mutex>

class Show {
public:
    enum Mode{
        REPEAT,      //repeat the show until say otherwise
        SINGLE_SHOT, //play the show from start to finish and stop
        STREAM       //move to wherever we are directed, without storing a show   
    };

    enum PlaybackState{
        SETUP,
        PLAY,
        PAUSE,
        STOP,
        EXITING
    };

    Show();
    ~Show();

    //prevent copy construction and assignment
    Show(const Show&) = delete;
    void operator=(const Show&) = delete;

    /**
     * @brief creates the hardware interface
     * 
     * @param filename 
     */
    void addHwInterface(std::string filename);

    //------------------------------PLAYBACK CONTROL------------------------------
    /**
     * @brief play the show
     * 
     */
    void play(void);

    /**
     * @brief pause the show, wherever it is
     * 
     */
    void pause(void);

    /**
     * @brief stop the show
     * 
     */
    void stop(void);

    /**
     * @brief seek to a specific point in the show
     * 
     * @param frameNum - the frame number to seek to
     */
    void seek(unsigned int frameNum);


    //------------------------------FRAME MANIPULATION------------------------------


    void addFrame(Frame f);

    void addFrame(Frame f, unsigned int loc);

    void removeFrame(unsigned int loc);

    void clear(void);


    //setters
    void setFrameRepeats(unsigned int num);
    void changeMode(Mode m);

    //getters
    size_t showLength(void);
    Frame currentFrame(void);
    unsigned int currentFrameNum(void);
    unsigned int frameRepeats(void);
    PlaybackState getPlaybackState(void);
    

private:

    void threadMain(void);
    void startPlayback(void);
    void stopPlayback(void);

    std::thread mPlaybackThread;
    
    std::atomic<unsigned int> mFrameNum;
    std::atomic<unsigned int> mNumFrameRepeats;
    std::atomic<Mode> mMode;
    std::atomic<PlaybackState> mPlaybackState;

    std::mutex mFramesMutex;
    std::vector<Frame> mFrames;

    std::unique_ptr<HwInterface> mpHWInterface;

};
#endif /* SHOW_H */
