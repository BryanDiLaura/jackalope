#ifndef PARAMETERS_H
#define PARAMETERS_H

#include <string>

namespace globals{

//used for stride calculations. Should not make any points larger than these values
const uint16_t MAX_X = 1024;
const uint16_t MAX_Y = 1024;

//the number of bits in the dac. (2^12 = 4096)
const uint16_t DAC_BITS = 12;

//the BBB pin number for the Laser control pin
const uint8_t LASER_PIN_NUM = 48;
const std::string LASER_PIN_STR(std::to_string(LASER_PIN_NUM));

//the BBB pin number for load-dac pin
const uint8_t LDAC_PIN_NUM = 31;
const std::string LDAC_PIN_STR(std::to_string(LDAC_PIN_NUM));

//amount of delay for the laser to move (microseconds)
const uint16_t MOVE_WAIT_US = 175;

//amount of delay for turning the laser on/off (microseconds)
const uint16_t LASER_POWER_WAIT_US = 500;

}

#endif /* PARAMETERS_H */
