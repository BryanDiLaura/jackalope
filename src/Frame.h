#ifndef FRAME_H
#define FRAME_H

#include "HwInterface.h"
#include "parameters.h"
#include <opencv2/opencv.hpp>

#include <vector>
#include <cstddef>

//used to notify when we need to toggle the laser in the frame
#define TOGGGLE_CORDS -1000

class Frame{
public:

    Frame();
    Frame(std::vector<std::vector<cv::Point> > cvFrame);

    ~Frame();

    /**
     * @brief draw the frame
     * 
     */
    void draw(void);

    /**
     * @brief add a point to the frame
     * 
     * @param p - the point to add
     */
    void addPoint(const cv::Point& p);

    /**
     * @brief adds a toggle point
     * 
     */
    void addToggle(void);

    /**
     * @brief remove a point from a specified index (0 indexed)
     * 
     * @param index where to remove the point
     */
    void removePoint(const unsigned int index);

    /**
     * @brief Get the point at the specified index (0 indexed)
     * 
     * @param index - the index in the frame to grab
     * @return Point - the point object
     */
    cv::Point getPoint(const unsigned int index);

    /**
     * @brief Get a reference to the Points object
     * 
     * @return std::vector<cv::Point>& 
     */
    std::vector<cv::Point>& getPoints(void);

    /**
     * @brief clear the frame
     * 
     */
    void clear(void);

    /**
     * @brief add a selection of points to the frame 
     * 
     * @param points - the list of points to add 
     */
    void addSelection(const std::vector<cv::Point>& points);

    /**
     * @brief same thing but it takes iterators...
     * 
     */
    void addSelection(std::vector<cv::Point>::iterator start, std::vector<cv::Point>::iterator end);

    /**
     * @brief add a selection of points to the frame at a specified location
     * 
     * @param points - the list of points to add
     * @param location - the index in the frame to insert it
     */
    void addSelection(const std::vector<cv::Point>& points, const unsigned int location);

    /**
     * @brief remove a selection of points from the frame
     * 
     * @param start - the beginning of the range to remove
     * @param end - the end of the range to remove 
     */
    void clearSection(const unsigned int start, const unsigned int end);

    /**
     * @brief delete a section of the frame and replace it with a selection of points
     * 
     * @param start - the beginning of the range to remove
     * @param end - the end of the range to remove
     * @param points - the list of points to add
     */
    void replaceSection(const unsigned int start, const unsigned int end, const std::vector<cv::Point>& points);

    /**
     * @brief get the overall length of the frame
     * 
     * @return size_t the length of the frame
     */
    size_t frameLength(void);
private:
    /**
     * @brief move where we are pointing to, performing boundary checking
     * 
     * @param pt - the point we are moving to, from the current position
     */
    void move(const cv::Point& pt);

    std::vector<cv::Point> mPoints;

};
#endif /* FRAME_H */
