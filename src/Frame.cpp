#include "Frame.h"

#include "jackalopeAssert.h"

using namespace std;
using namespace cv;

Frame::Frame(){

}

Frame::~Frame(){

}

Frame::Frame(std::vector<std::vector<cv::Point> > cvFrame){
    for (auto shape : cvFrame){
        //go to the first point an turn on the laser
        addPoint(shape.front());
        addPoint(Point(TOGGGLE_CORDS, TOGGGLE_CORDS));

        //add the remaining points and turn off
        addSelection(shape.begin()+1, shape.end());
        addPoint(Point(TOGGGLE_CORDS, TOGGGLE_CORDS));
    }
}

//-----------------------------------------------------------------------------
void Frame::draw(void){
    //draw the frame
}

//-----------------------------------------------------------------------------
void Frame::addPoint(const Point& p){
    mPoints.push_back(p);
}

//-----------------------------------------------------------------------------
void Frame::addToggle(void){
    mPoints.push_back(Point(TOGGGLE_CORDS, TOGGGLE_CORDS));
}

//-----------------------------------------------------------------------------
void Frame::removePoint(const unsigned int index){
    assert(index < mPoints.size());
    mPoints.erase(std::begin(mPoints) + index);
}

//-----------------------------------------------------------------------------
Point Frame::getPoint(const unsigned int index){
    assert(index < mPoints.size());
    return mPoints[index];
}

std::vector<cv::Point>& Frame::getPoints(void){
    return mPoints;
}

//-----------------------------------------------------------------------------
void Frame::clear(void){
    mPoints.clear();
}

//-----------------------------------------------------------------------------
void Frame::addSelection(const std::vector<Point>& points){
    mPoints.insert(std::end(mPoints), std::begin(points), std::end(points));
}

//-----------------------------------------------------------------------------
void Frame::addSelection(std::vector<Point>::iterator start, std::vector<Point>::iterator end){
    mPoints.insert(std::end(mPoints), start, end);
}

//-----------------------------------------------------------------------------
void Frame::addSelection(const std::vector<Point>& points, const unsigned int location){
    assert(location < mPoints.size());
    mPoints.insert(std::begin(mPoints) + location, std::begin(points), std::end(points));
}

//-----------------------------------------------------------------------------
void Frame::clearSection(const unsigned int start, const unsigned int end){
    assert(start <= end);
    assert(start < mPoints.size() && end < mPoints.size());
    mPoints.erase(std::begin(mPoints)+ start, std::begin(mPoints)+end);
}

//-----------------------------------------------------------------------------
void Frame::replaceSection(const unsigned int start, const unsigned int end, const std::vector<Point>& points){
    assert(start < end);
    assert(start < mPoints.size() && end < mPoints.size());
    clearSection(start, end);
    addSelection(points, start);
}

//-----------------------------------------------------------------------------
size_t Frame::frameLength(void){
    return mPoints.size();
}

//-----------------------------------------------------------------------------
void Frame::move(const Point& pt){
    //send hardware to specific location
}