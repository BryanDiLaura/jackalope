#include "Show.h"

#include "jackalopeAssert.h"

#include <iostream>
#include <chrono>
using namespace std;
using namespace std::chrono;

Show::Show()
    : mPlaybackThread(std::thread())
    , mFrameNum(0)
    , mNumFrameRepeats(1)
    , mMode(REPEAT)
    , mPlaybackState(SETUP)
{

}

Show::~Show(){
    stopPlayback();
}

void Show::addHwInterface(std::string filename){
    mpHWInterface = std::unique_ptr<HwInterface>(new HwInterface(filename));
}

//------------------------------------------------------------------------------
//------------------------------THREAD FUNCTIONS--------------------------------
//------------------------------------------------------------------------------
void Show::startPlayback(void){
    //if the thread was already created, destroy it and make a new one...
    stopPlayback();
    mPlaybackThread = std::thread(&Show::threadMain, this);
    mPlaybackState = PLAY;
}

//-----------------------------------------------------------------------------
void Show::stopPlayback(void){
    if (mPlaybackThread.joinable()){
        mPlaybackState = EXITING;
        mPlaybackThread.join();
    }
}

//------------------------------------------------------------------------------
void Show::threadMain(void){
    int frameRepeatCount = 0;
    Frame* pCurrentFrame;

    high_resolution_clock::time_point start;
    high_resolution_clock::time_point finish;
    int frameCount = 0;
    long long accum = 0;

    while(mPlaybackState != EXITING){
        //grab current frame
        {
        std::lock_guard<std::mutex> guard(mFramesMutex);
        pCurrentFrame = &mFrames[mFrameNum];
        }
        frameRepeatCount = 0;
        while(frameRepeatCount < mNumFrameRepeats){
            start = high_resolution_clock::now();
            for(auto point : pCurrentFrame->getPoints()){
                if(point.x == TOGGGLE_CORDS){
                    mpHWInterface->toggleLaser();
                }
                else{
                    mpHWInterface->pointAndCommit(point.x, point.y);
                }
            }
            finish = high_resolution_clock::now();

            accum += duration_cast<microseconds>(finish - start).count();
            frameCount++;
            if (frameCount % 100 == 0){
                cout << "average frame draw time = " << accum / 100.0 << "us" << endl;
                cout << "that's ~" << accum / 100.0 / 16.0 << "us per vertex" << endl;
                frameCount = 0;
                accum = 0;
            }

            frameRepeatCount++;
        }
    }
}

//------------------------------------------------------------------------------
//------------------------------PLAYBACK CONTROL------------------------------
//------------------------------------------------------------------------------
void Show::play(void){
    //if it hasn't already been started, start the playback thread.
    if (mPlaybackState == SETUP){
        startPlayback();
    }

    else if (mPlaybackState == STOP){
        mFrameNum = 0;
    }

    mPlaybackState = PLAY;
}

//------------------------------------------------------------------------------
void Show::pause(void){
    mPlaybackState = PAUSE;
}

//------------------------------------------------------------------------------
void Show::stop(void){
    mPlaybackState = STOP;
}

//------------------------------------------------------------------------------
void Show::seek(unsigned int frameNum){
    assert(frameNum < mFrames.size());
    mFrameNum = frameNum;
}

//------------------------------------------------------------------------------
//------------------------------FRAME MANIPULATION------------------------------
//------------------------------------------------------------------------------
void Show::addFrame(Frame f){
    std::lock_guard<std::mutex> guard(mFramesMutex);
    mFrames.push_back(f);
}

//------------------------------------------------------------------------------
void Show::addFrame(Frame f, unsigned int loc){
    assert(loc < mFrames.size());

    std::lock_guard<std::mutex> guard(mFramesMutex);
    auto it = mFrames.begin();
    mFrames.insert(it + loc, f);
}

//------------------------------------------------------------------------------
void Show::removeFrame(unsigned int loc){
    assert(loc < mFrames.size());

    std::lock_guard<std::mutex> guard(mFramesMutex);
    auto it = mFrames.begin();
    mFrames.erase(it + loc);
}

//------------------------------------------------------------------------------
void Show::clear(void){
    std::lock_guard<std::mutex> guard(mFramesMutex);
    mFrames.clear();
}


//setters
//------------------------------------------------------------------------------
void Show::setFrameRepeats(unsigned int num){
    mNumFrameRepeats = num;
}

void Show::changeMode(Mode m){
    mMode = m;
}

//getters
//------------------------------------------------------------------------------
size_t Show::showLength(void){
    std::lock_guard<std::mutex> guard(mFramesMutex);
    return mFrames.size();
}

//------------------------------------------------------------------------------
Frame Show::currentFrame(void){
    assert(mFrames.size() != 0);

    std::lock_guard<std::mutex> guard(mFramesMutex);
    return mFrames[mFrameNum];
}

//------------------------------------------------------------------------------
unsigned int Show::currentFrameNum(void){
    return mFrameNum;
}

//------------------------------------------------------------------------------
unsigned int Show::frameRepeats(void){
    return mNumFrameRepeats;
}

//------------------------------------------------------------------------------
Show::PlaybackState Show::getPlaybackState(void){
    return mPlaybackState;
}