#include <iostream>
#include <opencv2/opencv.hpp>
#include <chrono>
#include <thread>

#include "Show.h"
#include "Frame.h"
#include "HwInterface.h"

using namespace std;
using namespace cv;

using namespace std::this_thread;
using namespace std::chrono;
int main(void){

    cout << "Hello! Let's draw 4 squares!" << endl;
    
    Frame f; 
    f.addToggle();
    f.addPoint(Point(0,0));
    f.addPoint(Point(400,0));
    f.addPoint(Point(400,400));
    f.addPoint(Point(0,400));
    f.addPoint(Point(0,0));
    f.addToggle();

    f.addToggle();
    f.addPoint(Point(0,500));
    f.addPoint(Point(400,500));
    f.addPoint(Point(400,1000));
    f.addPoint(Point(0,1000));
    f.addPoint(Point(0,500));
    f.addToggle();

    f.addToggle();
    f.addPoint(Point(500,500));
    f.addPoint(Point(500,1000));
    f.addPoint(Point(1000,1000));
    f.addPoint(Point(1000,500));
    f.addPoint(Point(500,500));
    f.addToggle();

    f.addToggle();
    f.addPoint(Point(500,400));
    f.addPoint(Point(1000,400));
    f.addPoint(Point(1000,0));
    f.addPoint(Point(500,0));
    f.addPoint(Point(500,400));
    f.addToggle();

    Show s;
    s.addFrame(f);
    s.addHwInterface("/dev/i2c-2");


    cout << "Playing for 10 seconds..." << endl;
    s.play();

    sleep_for(seconds(10));

    cout << "Exiting!" << endl;
    s.stop();

    return 0;
}