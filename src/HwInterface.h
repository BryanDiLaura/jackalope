#ifndef HWINTERFACE_H
#define HWINTERFACE_H
// An abstraction for dealing with the details of the hardware.
// This also contains code for dealing with running the code without 
// being attached to actual hardware

#include "parameters.h"

#include <string>
#include <cstddef>
#include <cstdio>
#include <memory>
#include <math.h>
#include <string>
#include <opencv2/opencv.hpp>

/**
 * @brief This is a helper class for the HwInterface (below). It stores
 * both a positive and negative value, which are directly inversely 
 * correlated. The negative value is derived. For example (max is 101):
 * 
 * value    | positive  | negative
 * 0        | 0         | 100
 * 100      | 100       | 0
 * 50       | 50        | 50
 * 
 */
class DacValue{
public:
    /**
     * @brief sets the dac value, updating both the positive and negative values
     * 
     * @param val - the positive value to be set (negative will be derived)
     */
    void set(const unsigned int val);

    /**
     * @brief Get the positive value
     * 
     * @return uint16_t - the current positive value
     */
    uint16_t getPositive(void);

    /**
     * @brief Get the negative value
     * 
     * @return uint16_t - the current negative value
     */
    uint16_t getNegative(void);

private:
    uint16_t mMax = pow(2, globals::DAC_BITS) - 1; 
    uint16_t mPositive = mMax/2;
    uint16_t mNegative = mMax/2;
};


/**
 * @brief abstracts away talking to the i2c device. 
 * 
 */
class HwInterface{
public:

    /**
     * @brief Construct a new Hw Interface object. Construction may
     * fail if an invalid i2c device file is provided, or it was unable
     * to initialize the i2c interface correctly.
     * 
     * @param fileLocation - the location of the i2c device file (/dev/i2c-2 for 
     * the beaglebone black)
     */
    HwInterface(const std::string fileLocation);

    /**
     * @brief Destroy the Hw Interface object. This releases the i2c device file.
     * 
     */
    ~HwInterface(void);

    /**
     * @brief Sets the current internal values of the class to (x,y). This does 
     * nothing for pushing this to the hardware. 
     * 
     * @param x - the x location
     * @param y - the y location
     */
    void point(const unsigned int x, const unsigned int y);

    /**
     * @brief pushes current values to the hardware over i2c.
     * 
     */
    void commit(void);

    /**
     * @brief does both the point and commit functions in one operation
     * 
     * @param x - the x location
     * @param y - the y location
     */
    void pointAndCommit(const unsigned int x, const unsigned int y);

    /**
     * @brief toggle the laser status
     * 
     */
    void toggleLaser(void);

    /**
     * @brief Set the status of the laser
     * 
     * @param en - true = on, false = off
     */
    void setLaser(bool en);

    /**
     * @brief get the current laser status
     * 
     * @return true - the laser is turned on
     * @return false - the laser is turned off
     */
    bool laserStatus(void);

private:

    /**
     * @brief sets up the gpio for the system
     * 
     */
    void gpioSetup(void);

    DacValue mCurX;
    DacValue mCurY;

    bool mLaserStatus;

    const unsigned int mXLim = pow(2, globals::DAC_BITS);
    const unsigned int mXStep = pow(2, globals::DAC_BITS) / (globals::MAX_X - 1);
    const unsigned int mYLim = pow(2, globals::DAC_BITS);
    const unsigned int mYStep = pow(2, globals::DAC_BITS) / (globals::MAX_Y - 1);
    const std::string mFileLocation;
    int mFileDescriptor;
    int mLaserFileDescriptor;
    int mLdacFileDescriptor;
    uint16_t mOutBuff[4];
};
#endif /* HWINTERFACE_H */
