import numpy as np
import cv2


#--------------------read image--------------------
# image = "diamond.png"
# image = "checkers.jpg"
# image = "100.png"
# image = "gopro.png"
image = "lies.png"

#read in the image, and make a grayscale version
img = cv2.imread(image)
gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

#--------------------Create a binary image--------------------

#thresholding to only get stuff we care about
ret, thresh = cv2.threshold(gray, 127, 255, 0)
cv2.imshow("tresh", thresh)

#fancy adaptive thresholding for bad lighting conditions
thresh2 = cv2.adaptiveThreshold(gray, 255, cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY, 425, 2)
cv2.imshow("adaptive Thresh", thresh2)

#Otsu's thresholding with gaussian filter
ret, thresh3 = cv2.threshold(gray, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
cv2.imshow("otsu thresh", thresh3)

#same with a blur first, which helps apparently
# blur = cv2.GaussianBlur(gray, (5,5), 0)
blur = cv2.bilateralFilter(gray,9,75,75)
ret, thresh4 = cv2.threshold(blur, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
cv2.imshow("otsu thresh w/blur", thresh4)

#standard edge detection
edges = cv2.Canny(img, 100, 200)
cv2.imshow("edges", edges)

#--------------------Find contours--------------------

#find contours (can use either thresholded image or edges)
contours, hierarchy = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
cv2.drawContours(img, contours, -1, (255,0,0), 2)
print(f"there are {len(contours)} contours")

#get bounding rectanges for contours
# for c in contours:
#     x,y,w,h = cv2.boundingRect(c)
#     cv2.rectangle(img, (x,y), (x+w, y+h), (0,255,0), 2)

#--------------------approximate contours--------------------

#approximate contours with polygons
approxCnt = []
points = 0
for c in contours[1:]:
    epsilon = 0.001 * cv2.arcLength(c, True)
    approx = cv2.approxPolyDP(c, epsilon, cv2.isContourConvex(c))
    approxCnt.append(approx)
    points += len(approx)
cv2.drawContours(img, approxCnt, -1, (0,255,0), 2)
print(f"there are {points} points to draw")


cv2.imshow('image', img)
cv2.waitKey(0)
cv2.destroyAllWindows()