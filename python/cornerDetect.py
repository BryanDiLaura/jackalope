import cv2
import numpy as np

from fusePoints import * 

# image = "diamond.png"
# image = "triangle.png"
# image = "checkers.jpg"
# image = "100.png"
image = "lies.png"

#read in the image and convert to grayscale
img = cv2.imread(image)
img2 = img
gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

#perform edge detection
edges = cv2.Canny(img, 100, 200)

#perform corner detection
gray = np.float32(gray)
corn = cv2.cornerHarris(gray, 2, 3, 0.04)

points = np.argwhere(corn>0.01*corn.max())
points = fusePoints(points)

print(points)

corn = cv2.dilate(corn, None)
img[corn>0.01*corn.max()] = [0,0,255]



# cv2.imshow('image', img)
cv2.imshow('edges', edges)
cv2.waitKey(0)
cv2.destroyAllWindows()