from math import sqrt

def dist(p, q):
    return sqrt( (p[0] - q[0])**2 + (p[1] - q[1])**2 )

def fusePoints(points, d=2):
    ret = []
    n = len(points)
    #keep track of what we've used
    taken = [False] * n
    for i in range(n):
        if not taken[i]:
            point = [points[i][0], points[i][1]]
            taken[i] = True
            for j in range(i+1, n):
                if dist(points[i], points[j]) < d:
                    taken[j] = True
            ret.append((point[0], point[1]))
    return ret