import numpy as np
import cv2

#get the default video capture device
cap = cv2.VideoCapture(1)

while(True):
    # Capture frame-by-frame
    ret, frame = cap.read()

    # Our operations on the frame come here
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

    #threshold
    # ret, thresh = cv2.threshold(gray, 127, 255, 0)
    # cv2.imshow("tresh", thresh)

    # blur = cv2.GaussianBlur(gray, (5,5), 0)
    blur = cv2.bilateralFilter(gray,9,75,75)
    ret, thresh = cv2.threshold(blur, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
    cv2.imshow("otsu thresh w/blur", thresh)

    #find contours (can use either thresholded image or edges)
    contours, hierarchy = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

    #approximate
    approxCnt = []
    points = 0
    for c in contours:
        epsilon = 0.001 * cv2.arcLength(c, True)
        approx = cv2.approxPolyDP(c, epsilon, cv2.isContourConvex(c))
        approxCnt.append(approx)
        points += len(approx)
    cv2.drawContours(frame, approxCnt, -1, (0,255,0), 2)
    print(f"there are {points} points to draw")

    # Display the resulting frame
    cv2.imshow('frame',frame)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

# When everything done, release the capture
cap.release()
cv2.destroyAllWindows()